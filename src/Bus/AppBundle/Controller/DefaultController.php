<?php

namespace Bus\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('BusAppBundle:Default:index.html.twig');
    }
}
?>